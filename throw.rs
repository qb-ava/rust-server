use std::net::TcpStream;
use std::io::prelude::*;
use std::io::stdin;
fn main() -> std::io::Result<()> {
    let mut ipaddr = String::new();
    stdin().read_line(&mut ipaddr);
    println!("{}", &ipaddr.to_string());
    let mut stream = TcpStream::connect(&ipaddr.to_string().trim())?;
    let mut buffer = [0; 1000];
    stream.read(&mut buffer)?;
    stream.write(b"GET / HTTP/1.0\r\n\r\n"); 
    let mut info = String::new();
    println!("{:?}", buffer);

    Ok(())
}
